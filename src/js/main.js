document.addEventListener("DOMContentLoaded", function(event) {
    // Hamburger button
    const burger = document.querySelector('.hamburger');
    const nav = document.querySelector('.menu-box');
    burger.addEventListener('click', function(e){

        if (this.classList.contains('is-active')) {
            this.classList.remove('is-active');
            nav.classList.remove('menu-box-active');
        } else {
            this.classList.add('is-active');
            nav.classList.add('menu-box-active')
        }

    });

    // Massonry
    const grid = document.querySelector('.grid');
    const msnry = new Masonry(grid, {
        itemSelector: '.grid-item',
        gutter: 50,
        columnWidth: '.grid-sizer',
        isFitWidth: true,
        percentPosition: true
    });
});