const gulp = require('gulp');
const sass = require('gulp-sass');
const webserver = require('gulp-webserver');
const rigger = require('gulp-rigger');
const notify = require('gulp-notify');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify-es').default;
const pump = require('pump');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');

// Web server
gulp.task('webserver', function () {
    gulp.src('dist')
        .pipe(webserver({
            livereload: false,
            directoryListing: false,
            open: true
        }));
});

// HTML
gulp.task('html', function () {
    gulp.src('./src/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('dist/'))
        .pipe(notify({message: 'html task completed!'}));
});

// Sass
gulp.task('sass', function () {
    return gulp.src('./src/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./src/scss/**/*.scss', ['sass']);
});

// JavaScript
gulp.task('js', function (cb) {
    pump([
        gulp.src('./src/js/*.js'),
        uglify(),
        gulp.dest('./dist/js'),
        notify({ message: 'js task completed!'})
        ],
        cb
    );
});

// Vendors
gulp.task('js:vendors', function (cb) {
    pump([
        gulp.src('./src/js/vendors/*.js'),
        uglify(),
        concat('vendors.min.js'),
        gulp.dest('./dist/js/vendors/'),
        notify({ message: 'js task completed!'})
        ],
        cb
    );
});

// Images
gulp.task('images', function () {
    gulp.src('./src/images/**/*.*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest('./dist/images/'))
        .pipe(notify({ message: 'image task completed!'}));
});

// Watch
gulp.task('watch', function () {
    gulp.watch('./src/*.html', ['html']);
    gulp.watch('./src/scss/**/*.scss', ['sass']);
    gulp.watch('./src/js/*.js', ['js']);
    gulp.watch('./src/images/**/*.*', ['images']);
});

gulp.task('default', ['html', 'js:vendors', 'js', 'images', 'sass']);
gulp.task('serve', ['webserver', 'watch']);